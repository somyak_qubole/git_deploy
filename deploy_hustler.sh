#this script copies thefiles from hustler to web nodes. The files that are being copied are the ones changed more frequently by developers.
#Folders being copied - lib, bin and configs
#set -e
dir_name=$1
web_ip=$2
echo $dir_name
echo $web_ip
rsync -ar -e "ssh -i /usr/lib/qubole/shared/pemfiles/canopysshkey.pem" --exclude=$dir_name/hustler/lib/py/hive_scripts/. $dir_name/hustler/lib/. ec2-user@$web_ip:/usr/lib/hustler/lib/.
scp -i /usr/lib/qubole/shared/pemfiles/canopysshkey.pem -r $dir_name/hustler/bin/. ec2-user@$web_ip:/usr/lib/hustler/bin/.
scp -i /usr/lib/qubole/shared/pemfiles/canopysshkey.pem -r $dir_name/hustler/configs/. ec2-user@$web_ip:/usr/lib/hustler/configs/.

