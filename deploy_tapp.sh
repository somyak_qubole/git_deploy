#copy tapp and restart the nginx server.
#copy all folders excpet
dir_name=$1
web_ip=$2
rsync -ar -e "ssh -i /usr/lib/qubole/shared/pemfiles/canopysshkey.pem" --exclude=$dir_name/tapp/config/creds.yml $dir_name/tapp/. ec2-user@$web_ip:/home/ec2-user/src/qbol/tapp/.
ssh -i /usr/lib/qubole/shared/pemfiles/canopysshkey.pem -tt ec2-user@$web_ip "sudo /home/ec2-user/src/qbol/tapp/utils-bin/restart_nginx_dj.sh" < <(cat)
