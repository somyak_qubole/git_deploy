#copy hive_scripys folder
dir_name=$1
web_ip=$2
rsync -ar -e "ssh -i /usr/lib/qubole/shared/pemfiles/canopysshkey.pem" --exclude=$dir_name/hive_scripts/S3/*.py $dir_name/hive_scripts/. ec2-user@$web_ip:/usr/lib/hive_scripts/.
